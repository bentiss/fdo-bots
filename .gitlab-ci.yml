# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

.templates_sha: &template_sha 184ca628f89f3193c249b4e34e45afee2773a833

include:
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file:
      - '/templates/ci-fairy.yml'
      - '/templates/debian.yml'

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_PIPELINE_SOURCE == 'push'

variables:
  FDO_UPSTREAM_REPO: freedesktop/fdo-bots
  TESTING_DISTRIBUTION_TAG: '2023-06-29.0'
  TESTING_DISTRIBUTION_PACKAGES: 'python3-pip python3-venv git jq curl ssh'
  TESTING_DISTRIBUTION_VERSION: 'testing'
  GITLAB_TRIAGE_VERSION: 'v1.40.0'
  GIT_DEPTH: 1

.policy:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  # cancel run when a newer version is pushed to the branch
  interruptible: true

.not_on_upstream_main:
  rules:
    - if: ($CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH != $FDO_UPSTREAM_REPO)


fail-if-fork-is-not-public:
  extends:
    - .not_on_upstream_main
  script:
    - |
      if [ $CI_PROJECT_VISIBILITY != "public" ]; then
           echo "*************************************************************************************"
           echo "Project visibility must be set to 'public'"
           echo "Change this in $CI_PROJECT_URL/edit under 'Visibility, project features, permissions'"
           echo "*************************************************************************************"
           exit 1
      fi

#
# Verify that the merge request has the allow-collaboration checkbox ticked
#

check-merge-request:
  extends:
    - .fdo.ci-fairy
    - .not_on_upstream_main
  script:
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=results.xml
  artifacts:
    when: on_failure
    reports:
      junit: results.xml
  allow_failure: true

#
# Build distribution-specific images used by the jobs in the build stage
#

debian-testing@container-prep:
  extends:
    - .fdo.container-build@debian
    - .policy
  variables:
    GIT_STRATEGY: none
    FDO_DISTRIBUTION_TAG: $TESTING_DISTRIBUTION_TAG
    FDO_DISTRIBUTION_PACKAGES: $TESTING_DISTRIBUTION_PACKAGES
    FDO_DISTRIBUTION_VERSION: $TESTING_DISTRIBUTION_VERSION

gitlab-triage@container-prep:
  extends:
    - debian-testing@container-prep
  variables:
    FDO_BASE_IMAGE: $CI_REGISTRY_IMAGE/debian/$FDO_DISTRIBUTION_VERSION:$TESTING_DISTRIBUTION_TAG
    FDO_DISTRIBUTION_TAG: ${TESTING_DISTRIBUTION_TAG}-gitlab-triage-${GITLAB_TRIAGE_VERSION}
    FDO_DISTRIBUTION_PACKAGES: 'ruby'
    FDO_DISTRIBUTION_EXEC: 'gem install gitlab-triage'
  needs:
    - debian-testing@container-prep
