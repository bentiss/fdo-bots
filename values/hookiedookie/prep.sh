#!/bin/bash

set -x
set -e

PROJECT_REPO=${PROJECT_REPO:-$NAME}
SETTINGS=${SETTINGS:-${NAME}/Settings.tmpl}
SECRETS_FILE=${SECRETS_FILE:-${NAME}.yaml}

SHARED_DIR=/shared

# first, eventually clone the fdo-bots repository
FDO_BOTS=${SHARED_DIR}/fdo-bots
if [ ! -d ${FDO_BOTS} ]
then
  git clone git@gitlab.freedesktop.org:bentiss/fdo-bots.git $FDO_BOTS
  pushd $FDO_BOTS
  git submodule init
  popd
fi

# update fdo-bots
pushd $FDO_BOTS
git remote update
git reset --hard origin/main
git submodule update
popd

# clone the bot's project if required
BOT_PROJECT=${SHARED_DIR}/${NAME}
if [ ! -d ${BOT_PROJECT} ]
then
  git clone https://gitlab.freedesktop.org/freedesktop/${PROJECT_REPO}.git $BOT_PROJECT
fi

# create venv for the bot's project if required
BOT_PROJECT_VENV=${SHARED_DIR}/venv/${NAME}
if [ ! -d ${BOT_PROJECT_VENV} ]
then
  python3 -m venv ${BOT_PROJECT_VENV}
fi

# deploy the bot's project in a venv
pushd ${BOT_PROJECT}
source ${BOT_PROJECT_VENV}/bin/activate
git remote update
git reset --hard origin/main
pip install .
deactivate
popd

# create venv for jinja2 if required
JINJA2_VENV=${SHARED_DIR}/venv/jinja2
if [ ! -d ${JINJA2_VENV} ]
then
  python3 -m venv ${JINJA2_VENV}
  source ${JINJA2_VENV}/bin/activate
  pip install jinja2 jinja-cli
  deactivate
fi

# re-render our Settings.yaml
source ${JINJA2_VENV}/bin/activate
jinja -D SCRIPT_URL "$SCRIPT_URL" \
      -D PROJECT_REPO "$PROJECT_REPO" \
      -d ${FDO_BOTS}/helm-gitlab-secrets/${SECRETS_FILE} \
      -o ${SHARED_DIR}/Settings.yaml \
      ${FDO_BOTS}/values/${SETTINGS}
deactivate
