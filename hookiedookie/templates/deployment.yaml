apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "hookiedookie.fullname" . }}
  labels:
    {{- include "hookiedookie.labels" . | nindent 4 }}
spec:
  replicas: 1
  selector:
    matchLabels:
      {{- include "hookiedookie.selectorLabels" . | nindent 6 }}
  template:
    metadata:
    {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      labels:
        {{- include "hookiedookie.selectorLabels" . | nindent 8 }}
    spec:
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      initContainers:
        - name: pull-hookiedookie
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.path }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command:
            - "sh"
            - "-c"
          args:
            - "curl -L -o /shared/hookiedookie -f --retry 4 --retry-delay 60 $HOOKIE_URL && chmod +x /shared/hookiedookie"
          volumeMounts:
          - name: workdir
            mountPath: /shared
          env:
          - name: HOOKIE_URL
            value: "https://gitlab.freedesktop.org/api/v4/projects/freedesktop%2Fhookiedookie/packages/generic/hookiedookie/{{ .Chart.AppVersion }}/hookiedookie-x86_64"
        {{- if .Values.init_script }}
        - name: container-prep
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.path }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command:
            - "sh"
            - "-c"
          args:
            - "cd /shared && curl -LO -f --retry 4 --retry-delay 60 $SCRIPT_URL && chmod +x /shared/{{ .Values.init_script | base }} && /shared/{{ .Values.init_script | base }}"
          env:
          - name: SCRIPT_URL
            value: {{ .Values.init_script | quote }}
          - name: NAME
            value: {{ .Release.Name | quote }}
          - name: PROJECT_REPO
            value: {{ default .Release.Name .Values.project_repo | quote }}
          - name: SETTINGS
            value: {{ default (printf "%s/Settings.tmpl" .Release.Name) .Values.settings_template | quote }}
          - name: SECRETS_FILE
            value: {{ default (printf "%s.yaml" .Release.Name) .Values.secrets_file | quote }}
          volumeMounts:
          - name: workdir
            mountPath: /shared
          - name: ssh
            mountPath: /root/.ssh
            readOnly: true
        {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.path }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command:
            - "/shared/hookiedookie"
          args:
            - "{{ .Values.settings }}"
          ports:
            - containerPort: 8080
              name: http-web-svc
          env:
          - name: SCRIPT_URL
            value: {{ .Values.init_script | quote }}
          - name: NAME
            value: {{ .Release.Name | quote }}
          - name: PROJECT_REPO
            value: {{ default .Release.Name .Values.project_repo | quote }}
          - name: SETTINGS
            value: {{ default (printf "%s/Settings.tmpl" .Release.Name) .Values.settings_template | quote }}
          - name: SECRETS_FILE
            value: {{ default (printf "%s.yaml" .Release.Name) .Values.secrets_file | quote }}
          - name: GITLAB_TOKEN
            valueFrom:
              secretKeyRef:
                key: auth_token
                name: {{ include "hookiedookie.fullname" . }}
          volumeMounts:
          - name: workdir
            mountPath: /shared
          - name: ssh
            mountPath: /root/.ssh
            readOnly: true
          livenessProbe:
            httpGet:
              path: /healthz
              port: 8080
            initialDelaySeconds: 3
            periodSeconds: 10
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
      - name: workdir
        emptyDir: {}
      - name: ssh
        secret:
          secretName: {{ include "hookiedookie.fullname" . }}
          defaultMode: 0400
